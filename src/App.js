import React from 'react';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          camera: "",
          customers: 0,
          stocking: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.submitValues = this.submitValues.bind(this);
    }

    ws = new WebSocket("ws://127.0.0.1:3030/params?ID=ABAB5&type=input");

    componentDidMount() {
      this.ws.onopen = () => {
        // If connected, log to console
        console.log("connected")
      }

      this.ws.onmessage = evt => {
        console.log(evt)
        this.setState({
          message: evt.data
        })
      }
    }

    // Auto save the submitted data in the state of the class.
    handleChange(event) {
      const {name, value} = event.target;
      this.setState({
      [name]: value
      });
    }

    submitValues() {
      const msg = { customerCount: this.state.customers, stockingStatus: this.state.stocking }
      this.ws.send(JSON.stringify(msg))
    }

    render() {
        return (
          <div className="App">
              <div className="wrapper">
                <select onChange={this.handleChange} name="camera" id="cameras">
                  <option value="" disabled selected>Kies een camera</option>
                  <option value="Fruit">Fruit</option>
                  <option value="Groente">Groente</option>
                  <option value="Chips">Chips</option>
                  <option value="Drinken">Drinken</option>
                </select>

                <select onChange={this.handleChange} name="customers" id="customers">
                  <option value="" disabled selected> Kies het aantal klanten </option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                </select>

                <select onChange={this.handleChange} name="stocking" id="stocking">
                  <option value="" disabled selected> Zijn er vulkwerkzaamheden bezig? </option>
                  <option value={true}>Ja, er zijn vulwerkzaamheden </option>
                  <option value={false}>Nee, geen vulwerkzaamheden </option>
                </select>

                <button onClick={this.submitValues}> Submit new values </button>
              </div>
          </div>
        );
    }
}

export default App;